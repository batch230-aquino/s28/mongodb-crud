//CRUD of operations 
/*
    -CRUD operations are the heart of any backend applications
    -mastering the CRUD operations is essential for any developer
    -this helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    -mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/


// [SECTION] inserting Documents (Create)
/*
    syntax:
            - db.collectionName.insertOne({object});
*/

//Insert One (insert one document)

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact:{
        phone: "87654321",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "JavaScript", "Phyton"],
    department: "none"
});

// Insert Many (insert one or more documents)
/*
    -Syntax
    - db.collectionName.insertMany([{objectA}, {objectB}])
*/ 
db.users.insertMany(
    [
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact:{
            phone: "87000",
            email: "stephenhawking@mail.com"
        },
        courses: ["Phyton", "React", "PHP"],
        department: "none"

    },
    {
        firstName: "Niel",
        lastName: "Armstrong",
        age: 82,
        contact:{
            phone: "4770220",
            email: "nielarmstrong@mail.com"
        },
        courses: ["React", "Lavarel", "MongoDB"],
        department: "none"

    }

    ]
);

//[SECTION] Finding Documents
/*
    - db.collectionName.find();
    - db.collectionName.find({field : value});
*/

//retreives all documents
db.users.find();
//retrieves specific documents
db.users.find({firstName: "Stephen"});
//multiple criteria
db.users.find({lastName: "Armstrong", age:70});



  //[SECTION] UPDate and replace

  db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

// syntax:
/**
db.collectionName.updateOne({criteria}. {$set: {field/s : value/s})
*/

db.users.updateOne(
    {firstName: "Test"},
    {
        $set:{
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact:{
                phone: "12345678",
                email: "imrich@mail.com"
            },
            courses: ["PHP", "Lavarel", "HTML"],
            department: "Operations",
            status: "active"
        }
    }
)

db.users.find();



db.users.updateOne(
{firstName: "Jane"},
{
    $set: 
    {
        firstName:"Jenny"
    }
}
)

//updating multiple documents that matches field/ criterea
db.users.updateMany(
{department: "none"},
{
    $set:{
        department: "HR"
    }
}
)

//REPLACE replaces all the field in a document/object
db.users.replaceOne(
    {firstName: "Bill"}, //criteria
    {
        firstName: "Elon",
        lastName: "Musk",
        age:30,
        courses: ["PHP", "Lavarel", "HTML"],
        status: "trippings"
    }
);
//[SECTION] delete(delete)
/*
    db.collectionName.DeleteOne({field : value})
    db.collectionName.DeleteMany({field : value})
 */

    db.users.deleteOne({
        firstname: "Jane"
    });


    db.users.deleteMany({
        firstName: "HR";
    });

    db.users.deleteMany({
        firstName: "Niel";
    });



  